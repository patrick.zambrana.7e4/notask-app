package com.example.notask

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList

class ViewModel: ViewModel(){
    private val apiInterface = ApiInterface.create()

    var user :Usuari? = null
    var id = "0"
    var llistes = MutableLiveData<List<Llista>>()
    var currentTasques = MutableLiveData<List<Tasca>>()
    var llistaUsuaris = MutableLiveData<List<Usuari>>()

    fun getLlistesApi(){
        val callLlistes = apiInterface.getLists(id)
        callLlistes.enqueue(object: Callback<List<Llista>> {
            override fun onFailure(call: Call<List<Llista>>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<List<Llista>>, response: Response<List<Llista>>) {
                if (response.isSuccessful) {
                    llistes.postValue(response.body())
                }
            }
        })
    }

    fun getLlista(id:Long): Llista? {
        for (llista in llistes.value!!){
            if (llista.idLista == id)
                return llista
        }
        return null
    }

    fun createList(name:String){
        val list = Llista(null,name, ArrayList())
        val addList = apiInterface.addList(id,list)
        addList.enqueue(object: Callback<Llista>{
            override fun onFailure(call: Call<Llista>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {
                if (response.isSuccessful) {
                    getLlistesApi()
                    Log.i("text", "added")
                }
            }
        })
    }

    fun deleteList(idList:Long){
        val deleteList = apiInterface.deleteList(id,idList)
        deleteList.enqueue(object: Callback<Llista>{
            override fun onFailure(call: Call<Llista>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {

            }
        })
    }

    fun addTasca(tasca: Tasca, llistaId:Long){
        val addTasca = apiInterface.addTask(id,llistaId, tasca)

        addTasca.enqueue(object :Callback<Tasca>{
            override fun onFailure(call: Call<Tasca>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Tasca>, response: Response<Tasca>) {
                getTasquesApi(llistaId)
            }
        })
    }

    fun editList(llista: Llista){
        val updateList = apiInterface.updateList(id,llista)
        updateList.enqueue(object :Callback<Llista>{
            override fun onFailure(call: Call<Llista>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {

            }
        })
    }

    fun deleteTasca(llistaId: Long, tasca: Tasca){
        val deleteTasca = apiInterface.deleteTask(id,llistaId, tasca.idTasca)
        deleteTasca.enqueue(object : Callback<Llista>{
            override fun onFailure(call: Call<Llista>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {
                getLlista(llistaId)!!.tasques.remove(tasca)
            }
        })
    }

    fun editTasca(llistaId: Long, tasca: Tasca){
        val updateTasca = apiInterface.updateTask(id,llistaId, tasca)
        updateTasca.enqueue(object : Callback<Llista>{
            override fun onFailure(call: Call<Llista>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {
            }
        })
    }

    fun getTasquesApi(llistaId: Long): List<Tasca>{
        var tasques = mutableListOf<Tasca>()
        val updateTasques = apiInterface.getListTasks(id,llistaId)
        updateTasques.enqueue(object  : Callback<List<Tasca>>{
            override fun onFailure(call: Call<List<Tasca>>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }

            override fun onResponse(call: Call<List<Tasca>>, response: Response<List<Tasca>>) {
               currentTasques.postValue(response.body())
            }
        })
        return tasques
    }

    // ACC MANAGER

    fun getLlistaUsuaris() {
        val callUsuaris = apiInterface.llistarUsuaris()
        callUsuaris.enqueue(object: Callback<List<Usuari>> {
            override fun onFailure(call: Call<List<Usuari>>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<List<Usuari>>, response: Response<List<Usuari>>) {
                if (response.isSuccessful) {
                    llistaUsuaris.postValue(response.body())
                    Log.i("test", "${response.body()}")
                }
            }
        })
    }

    fun createUser(userName: String, password: String){
        val user = Usuari(null, userName, password, null)
        val addUser = apiInterface.crearUsuari(user)
        addUser.enqueue(object: Callback<Usuari>{
            override fun onFailure(call: Call<Usuari>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Usuari>, response: Response<Usuari>) {
                if (response.isSuccessful) {
                    getLlistaUsuaris()
                }
            }
        })
    }

    fun consultarUser(){
        val callUser = apiInterface.consultarUsuari(id)
        callUser.enqueue(object: Callback<Usuari> {
            override fun onFailure(call: Call<Usuari>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }

            override fun onResponse(call: Call<Usuari>, response: Response<Usuari>) {
                if (response.isSuccessful){
                    user = response.body()
                }
            }
        })
    }
}