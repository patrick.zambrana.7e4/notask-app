package com.example.notask


import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import retrofit2.http.DELETE

interface ApiInterface {

    @GET("users/{idUsuari}/todolists")
    fun getLists(@Path("idUsuari") idUsuari: String): Call<List<Llista>>

    @GET("users/{idUsuari}/todolists/{idList}/todoitems")
    fun getListTasks(@Path("idUsuari") idUsuari: String,@Path("idList") id: Long): Call<List<Tasca>>

    @GET("users/{idUsuari}/todolists/{idList}/todoitems/{idTask}")
    fun getTask(@Path("idUsuari") idUsuari: String,@Path("idList") idList: Long, @Path("idTask") idTask:Long): Call<Llista>

    @POST("users/{idUsuari}/todolists")
    fun addList(@Path("idUsuari") idUsuari: String,@Body list: Llista?): Call<Llista>

    @POST("users/{idUsuari}/todolists/{idList}/todoitems")
    fun addTask(@Path("idUsuari") idUsuari: String,@Path("idList") idList:Long, @Body task: Tasca?): Call<Tasca>

    @PUT("users/{idUsuari}/todolists")
    fun updateList(@Path("idUsuari") idUsuari: String, @Body list: Llista): Call<Llista>

    @PUT("users/{idUsuari}/todolists/{idList}/todoitems")
    fun updateTask(@Path("idUsuari") idUsuari: String,@Path("idList") idList: Long, @Body task: Tasca): Call<Llista>

    @DELETE("users/{idUsuari}/todolists/{idList}")
    fun deleteList(@Path("idUsuari") idUsuari: String,@Path("idList") idList: Long): Call<Llista>

    @DELETE("users/{idUsuari}/todolists/{idList}/todoitems/{idTask}")
    fun deleteTask(@Path("idUsuari") idUsuari: String,@Path("idList") idList: Long, @Path("idTask") idTask: Long):Call<Llista>


    // ACC MANAGER

    @GET("users")
    fun llistarUsuaris(): Call<List<Usuari>>

    @GET("users/{idUsuari}")
    fun consultarUsuari(@Path("idUsuari") idUsuari: String): Call<Usuari>

    @POST("users")
    fun crearUsuari(@Body user: Usuari): Call<Usuari>


    companion object {
        var URL = "https://no-task.herokuapp.com/"
        private var BASE_URL = URL
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}