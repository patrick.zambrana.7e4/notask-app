package com.example.notask.spinner

import android.content.Context
import android.graphics.drawable.Drawable
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.notask.R

class SpinnerAdapter(

    var contxt : Context

) : BaseAdapter() {

    private var context = contxt
    private var iconList = listOf(R.drawable.edit, R.drawable.delete)

    override fun getCount(): Int {
        if (iconList != null)
            return iconList.size
        else
            return 0
    }

    override fun getItem(i: Int): Any {
        return i
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup?): View {
        var rootView = LayoutInflater.from(context)
            .inflate(R.layout.item_option, viewGroup, false)

        var eliminar = rootView.findViewById<TextView>(R.id.spinner_eliminar)
        var editar = rootView.findViewById<TextView>(R.id.spinner_editar)

        return rootView
    }

}