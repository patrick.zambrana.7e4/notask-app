package com.example.notask

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class FragmentLists : Fragment(R.layout.lists_fragment) {
    lateinit var recyclerView: RecyclerView
    lateinit var addButton: AppCompatButton
    lateinit var dialog: Dialog
    private val viewModel:ViewModel by activityViewModels()
    private lateinit var signUp: ImageView
    private lateinit var nameUser : TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = ListAdapter(viewModel)

        viewModel.consultarUser();
        viewModel.getLlistesApi()


        recyclerView = view.findViewById(R.id.lists_recycler_view)
        addButton = view.findViewById(R.id.add_list_button)
        dialog = Dialog(context as MainActivity)
        signUp = view.findViewById(R.id.to_sign_up_btn)
        nameUser = view.findViewById(R.id.nameUser)

        recyclerView.layoutManager = LinearLayoutManager(this.requireContext())
        recyclerView.adapter = adapter

        viewModel.llistes.observe(viewLifecycleOwner, {
            adapter.setLists(it)
        })

        addButton.setOnClickListener {
            showDialog(adapter)
        }

        if (!viewModel.id.equals("0")){
            signUp.setImageResource(R.drawable.go_back)
            signUp.setOnClickListener {
                viewModel.id = "0";
                NavHostFragment.findNavController(this)
                    .navigate(R.id.action_fragmentLists_to_loginFragment)
            }
        }else {
            signUp.setOnClickListener {
                NavHostFragment.findNavController(this)
                    .navigate(R.id.action_fragmentLists_to_loginFragment)
            }
        }

        Handler().postDelayed({
            if (viewModel.user?.idUsuari?.equals("0") != true){
                nameUser.setText(viewModel.user?.userName)
            }
        },500)

    }

    fun showDialog(adapter: ListAdapter) {
        dialog.setContentView(R.layout.custom_dialog)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        var editText = dialog.findViewById<EditText>(R.id.dialog_new_name)
        var cancel = dialog.findViewById<Button>(R.id.dialog_cancel_button)
        var okButton = dialog.findViewById<Button>(R.id.dialog_ok_button)

        val size = adapter.llistes.size+1
        val defaultName = "Llista $size"
        editText.setText(defaultName)

        okButton.setOnClickListener {
            val name = editText?.text.toString()
            viewModel.createList(name)
            dialog.dismiss()
        }

        cancel?.setOnClickListener {
            dialog.dismiss()
        }
        dialog.window?.setDimAmount(0F)
        dialog.show()
    }
}