package com.example.notask

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.NavHostFragment

class LoginFragment : Fragment(R.layout.login_fragment){

    private lateinit var username: EditText
    private lateinit var password: EditText
    private lateinit var loginBtn: Button
    private lateinit var registerBtn: TextView
    private val viewModel:ViewModel by activityViewModels()
    //val adapter = ListAdapter(viewModel)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        username = view.findViewById(R.id.username_input)
        password = view.findViewById(R.id.password_input)
        loginBtn = view.findViewById(R.id.login_button)
        registerBtn = view.findViewById(R.id.register_btn)

        viewModel.getLlistaUsuaris()

        registerBtn.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_registerFragment)
        }

        loginBtn.setOnClickListener {
            var find = false
            if (checkData()) {
                for (usuari in viewModel.llistaUsuaris.value!!) {
                    if (username.text.contentEquals(usuari.userName)) {
                        Toast.makeText(context, "Login correcte!", Toast.LENGTH_SHORT).show()
                        viewModel.id = usuari.idUsuari.toString();
                        NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_fragmentLists)
                        find = true
                    }
                }
                if (!find)
                    Toast.makeText(context, "Les dades no coincideixen amb cap usuari.", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, "Les dades son incorrectes.\n" +
                        "Han de contindre almenys 4 caràcters.", Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun checkData() : Boolean {
        return !(username.text.trim().isEmpty() || username.text.contains(" ") || username.text.length < 4 ||
                password.text.trim().isEmpty() || password.text.contains(" ") || password.text.length < 4)
    }

}