package com.example.notask

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import androidx.annotation.NonNull

import android.R.string.no
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback


class TasquesFragment : Fragment(R.layout.tasques_fragment) {
    lateinit var recyclerView: RecyclerView
    lateinit var recyclerViewDone: RecyclerView
    lateinit var addButton: AppCompatButton
    lateinit var llistName: TextView
    lateinit var goBack: ImageView
    lateinit var dialog: Dialog

    private val viewModel: ViewModel by activityViewModels()
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.tasks_recycler_view)
        recyclerViewDone = view.findViewById(R.id.tasks_recycler_view_done)
        addButton = view.findViewById(R.id.add_task_button)
        llistName = view.findViewById(R.id.llist_detail_name)
        goBack = view.findViewById(R.id.go_back_button)
        dialog = Dialog(context as MainActivity)
        val listId = TasquesFragmentArgs.fromBundle(requireArguments()).listId

        val adapter = TaskAdapter(viewModel.getLlista(listId)!!.tasques, viewModel, listId)
        recyclerView.layoutManager = LinearLayoutManager(this.requireContext())
        recyclerView.adapter = adapter

        val adapterDone = TaskAdapter(viewModel.getLlista(listId)!!.tasques, viewModel, listId)
        recyclerViewDone.layoutManager = LinearLayoutManager(this.requireContext())
        recyclerViewDone.adapter = adapterDone

        val linearLayout: LinearLayout = view.findViewById(R.id.done_tasks)
        bottomSheetBehavior = BottomSheetBehavior.from(linearLayout)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED


        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                viewModel.getTasquesApi(listId)
            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

        llistName.text = viewModel.getLlista(listId)!!.nom

        val swipeGesture = object : SwipeGesture(context as MainActivity) {

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                when (direction){
                    ItemTouchHelper.LEFT -> {
                        adapter.deleteItem(viewHolder.adapterPosition)
                    }
                    ItemTouchHelper.RIGHT -> {
                        //val archiveItem = llistName[viewHolder.adapterPosition]
                        //adapter.deleteItem(viewHolder.adapterPosition)
                    }
                }

                //super.onSwiped(viewHolder, direction)
            }
        }

        val touchHelper = ItemTouchHelper(swipeGesture)
        touchHelper.attachToRecyclerView(recyclerView)


        goBack.setOnClickListener {
            findNavController(this).navigate(R.id.action_tasquesFragment_to_fragmentLists)
        }

        addButton.setOnClickListener {
            showDialog(adapter)
        }

        viewModel.currentTasques.observe(viewLifecycleOwner,{
            adapter.updateTasques(it)
            adapterDone.filterDone(it)
        })
    }

    fun showDialog(adapter: TaskAdapter) {
        dialog.setContentView(R.layout.custom_dialog)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        var editText = dialog.findViewById<EditText>(R.id.dialog_new_name)
        var cancel = dialog.findViewById<Button>(R.id.dialog_cancel_button)
        var okButton = dialog.findViewById<Button>(R.id.dialog_ok_button)
        var title = dialog.findViewById<TextView>(R.id.dialog_list_name)
        val listId = TasquesFragmentArgs.fromBundle(requireArguments()).listId
        llistName.text = viewModel.getLlista(listId)!!.nom
        val size = adapter.tasques.size+1
        title.text = "Nom de la tasca"

        val defaultName = "Tasca $size"
        editText.setText(defaultName)

        okButton.setOnClickListener {
            val name = editText.text.toString()
            adapter.addTask(name)
            dialog.dismiss()
        }

        cancel?.setOnClickListener {
            dialog.dismiss()
        }
        dialog.window?.setDimAmount(0F)
        dialog.show()
    }
}