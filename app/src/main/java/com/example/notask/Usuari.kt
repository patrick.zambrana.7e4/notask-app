package com.example.notask

data class Usuari (
    var idUsuari: Long?,
    var userName: String,
    var password: String,
    var listas: List<Llista>?
        )