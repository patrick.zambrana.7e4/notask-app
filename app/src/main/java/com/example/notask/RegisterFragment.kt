package com.example.notask

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment

class RegisterFragment : Fragment(R.layout.register_fragment) {

    private lateinit var setUsername : EditText
    private lateinit var setPassword : EditText
    private lateinit var confirmPassword: EditText
    private lateinit var registerButton: Button
    private lateinit var loginBtn: TextView
    private val viewModel:ViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUsername = view.findViewById(R.id.username_input)
        setPassword = view.findViewById(R.id.password_input)
        confirmPassword = view.findViewById(R.id.password_input_confirm)
        registerButton = view.findViewById(R.id.register_button)
        loginBtn = view.findViewById(R.id.login_btn)

        registerButton.setOnClickListener {
            if (checkData()) {
                if (setPassword.text.toString() == confirmPassword.text.toString()) {
                    viewModel.createUser(setUsername.text.toString(), setPassword.text.toString())
                    Toast.makeText(context, "Compte creat.", Toast.LENGTH_SHORT).show()
                    NavHostFragment.findNavController(this)
                        .navigate(R.id.action_registerFragment_to_loginFragment)
                }else{
                    Toast.makeText(context, "Les contrasenyes sno son iguals", Toast.LENGTH_LONG)
                        .show()
                }
            } else {
                Toast.makeText(context, "Les dades son incorrectes.\n" +
                        "Han de contindre almenys 4 caràcters.", Toast.LENGTH_LONG).show()
            }
        }

        loginBtn.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_registerFragment_to_loginFragment)
        }
    }

    private fun checkData() : Boolean {
        return !(setUsername.text.trim().isEmpty() || setUsername.text.contains(" ") || setUsername.text.length < 4 ||
                setPassword.text.trim().isEmpty() || setPassword.text.contains(" ") || setPassword.text.length < 4)
    }
}