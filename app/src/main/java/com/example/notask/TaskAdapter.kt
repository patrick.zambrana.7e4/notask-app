package com.example.notask

import android.app.Dialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView

class TaskAdapter(var tasques:MutableList<Tasca>, val viewModel: ViewModel, val llistaId: Long) : RecyclerView.Adapter<TaskAdapter.TaskViewHolder>() {
    fun updateTasques(list: List<Tasca>){
        tasques = list.filter { tasca -> !tasca.feta }.toMutableList()
        notifyDataSetChanged()
    }

    fun filterDone(list: List<Tasca>){
        tasques = list.filter { tasca -> tasca.feta }.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_tasca, parent, false)
        return TaskViewHolder(view)
    }

    fun addTask(name: String){
        val tasca = Tasca(0,name, name, false)
        tasques.add(tasca)
        viewModel.addTasca(tasca, llistaId)
        notifyDataSetChanged()
    }

    // Swipe
    fun deleteItem(id: Int) {
        viewModel.deleteTasca(llistaId, tasques[id])
        tasques.removeAt(id)
        notifyDataSetChanged()
    }

    // --- -- -- --

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bindData(tasques[position])
    }

    override fun getItemCount(): Int {
        return tasques.size
    }

    inner class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var taskName: TextView = itemView.findViewById(R.id.task_name)
        private var editButton: ImageView = itemView.findViewById(R.id.editar_tasca)
        private var deleteButton: ImageView = itemView.findViewById(R.id.eliminar_tasca)
        private var checkBox: CheckBox = itemView.findViewById(R.id.check_box)

        fun bindData(tasca: Tasca) {
            taskName.text = tasca.nom
            checkBox.isChecked = tasca.feta

            checkBox.setOnClickListener {
                tasca.feta = !tasca.feta
                viewModel.editTasca(llistaId, tasca)
                tasques.remove(tasca)
                notifyDataSetChanged()
            }

            editButton.setOnClickListener {
                var dialog = Dialog(itemView.context as MainActivity)
                dialog.setContentView(R.layout.custom_dialog)
                dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

                var editText = dialog.findViewById<EditText>(R.id.dialog_new_name)
                var cancel = dialog.findViewById<Button>(R.id.dialog_cancel_button)
                var okButton = dialog.findViewById<Button>(R.id.dialog_ok_button)
                var title = dialog.findViewById<TextView>(R.id.dialog_list_name)
                title.text = "Nou nom de la tasca"
                editText.setText(tasca.nom)

                okButton.setOnClickListener {
                    val newName = editText.text.toString()
                    tasca.nom = newName
                    notifyDataSetChanged()
                    viewModel.editTasca(llistaId, tasca)
                    dialog.dismiss()
                }

                cancel?.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.window?.setDimAmount(0F)
                dialog.show()
            }

            deleteButton.setOnClickListener {
                var dialog = Dialog(itemView.context as MainActivity)
                dialog.setContentView(R.layout.custom_dialog_warning)
                dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

                var cancel = dialog.findViewById<Button>(R.id.dialog_cancel_button)
                var okButton = dialog.findViewById<Button>(R.id.dialog_ok_button)
                var title = dialog.findViewById<TextView>(R.id.dialog_confirmation)
                title.text = "Estás segur de voler eliminar la tasca?"

                okButton.setOnClickListener {
                    tasques.remove(tasca)
                    notifyDataSetChanged()
                    viewModel.deleteTasca(llistaId, tasca)
                    dialog.dismiss()
                }

                cancel?.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.window?.setDimAmount(0F)
                dialog.show()
            }
        }
    }
}