package com.example.notask

import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import android.widget.Button

import android.widget.EditText

class ListAdapter(val viewModel: ViewModel) : RecyclerView.Adapter<ListAdapter.ListViewHolder>() {
    var llistes = mutableListOf<Llista>()

    fun setLists(lists: List<Llista>){
        llistes = lists.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_llista, parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bindData(llistes[position])
    }

    override fun getItemCount(): Int {
        return llistes.size
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var listName: TextView = itemView.findViewById(R.id.list_name)
        private var editButton: ImageView = itemView.findViewById(R.id.editar_llista)
        private var deleteButton: ImageView = itemView.findViewById(R.id.eliminar_llista)

        fun bindData(llista: Llista) {
            listName.text = llista.nom
            itemView.setOnClickListener {
                llista.idLista?.let { it1 -> viewModel.getTasquesApi(it1) }
                //viewModel.currentTasques.postValue(llista.tasques)
                val action = llista.idLista?.let { it1 ->
                    FragmentListsDirections.actionFragmentListsToTasquesFragment(
                        it1
                    )
                }
                if (action != null) {
                    findNavController(itemView).navigate(action)
                }
            }

            editButton.setOnClickListener {
                var dialog = Dialog(itemView.context as MainActivity)
                dialog.setContentView(R.layout.custom_dialog)
                dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

                var editText = dialog.findViewById<EditText>(R.id.dialog_new_name)
                var cancel = dialog.findViewById<Button>(R.id.dialog_cancel_button)
                var okButton = dialog.findViewById<Button>(R.id.dialog_ok_button)
                var title = dialog.findViewById<TextView>(R.id.dialog_list_name)
                title.text = "Editar llista"
                editText.setText(llista.nom)

                okButton.setOnClickListener {
                    val newName = editText.text.toString()
                    llista.nom = newName
                    notifyDataSetChanged()
                    viewModel.editList(llista)
                    dialog.dismiss()
                }

                cancel?.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.window?.setDimAmount(0F)
                dialog.show()
            }

            deleteButton.setOnClickListener {

                var dialog = Dialog(itemView.context as MainActivity)
                dialog.setContentView(R.layout.custom_dialog_warning)
                dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

                var cancel = dialog.findViewById<Button>(R.id.dialog_cancel_button)
                var okButton = dialog.findViewById<Button>(R.id.dialog_ok_button)

                okButton.setOnClickListener {
                    llistes.remove(llista)
                    notifyDataSetChanged()
                    llista.idLista?.let { it1 -> viewModel.deleteList(it1) }
                    dialog.dismiss()
                }

                cancel?.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.window?.setDimAmount(0F)
                dialog.show()
            }
        }
    }

}