package com.example.notask

data class Llista(
    var idLista: Long?,
    var nom: String,
    var tasques: ArrayList<Tasca>)
{
    var type:String = nom
}